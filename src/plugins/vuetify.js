import Vue from 'vue'
import Vuetify from 'vuetify/lib'

Vue.use(Vuetify, {
  iconfont: 'md',
  theme: {
    primary: '#03a9f4',
    secondary: '#2196f3',
    accent: '#3f51b5',
    error: '#f44336',
    warning: '#ffc107',
    info: '#8bc34a',
    success: '#4caf50',
  },
});
